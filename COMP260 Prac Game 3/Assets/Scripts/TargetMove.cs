﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

    public float StartTime = 0.0f;
    private Animator TargetAnimator;

	// Use this for initialization
	void Start () {
        TargetAnimator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time >= StartTime)
        {
            TargetAnimator.SetTrigger("Start");
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        TargetAnimator.SetTrigger("Hit");
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }


}
