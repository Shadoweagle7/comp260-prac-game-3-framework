﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellMenu : MonoBehaviour {

    public GameObject ShellPanel;
    private bool Paused = true;

    private void SetPaused(bool paused)
    {
        Paused = paused;
        ShellPanel.SetActive(paused);
        Time.timeScale = Paused ? 0 : 1;
    }

	// Use this for initialization
	void Start () {
        SetPaused(true);
	}
	
	// Update is called once per frame
	void Update () {
        if (!Paused && Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(true);
        }
	}

    public void OnPressedPlay()
    {
        SetPaused(false);
    }

    public void OnPressedQuit()
    {
        Debug.Log("Normally Quitting Application Here...");
        Application.Quit();
    }
}
