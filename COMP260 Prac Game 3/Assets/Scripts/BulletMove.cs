﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    public float Speed = 10.0f;
    public Vector3 Direction;
    private Rigidbody BulletRigidBody;

	// Use this for initialization
	void Start () {
        BulletRigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        BulletRigidBody.velocity = Speed * Direction;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Destroying object " + gameObject);
        Destroy(gameObject);
    }
}
