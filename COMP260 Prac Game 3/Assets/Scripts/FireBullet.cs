﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

    public BulletMove BulletPrefab;

    public float TimeSinceLastBulletFired;

	// Use this for initialization
	void Start () {
        TimeSinceLastBulletFired = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeScale == 0)
        {
            return;
        }

        if (Input.GetButtonDown("Fire1") && Time.time - TimeSinceLastBulletFired >= 0.001)
        {
            TimeSinceLastBulletFired = Time.time;
            BulletMove bullet = Instantiate(BulletPrefab);

            bullet.transform.position = transform.position;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            bullet.Direction = ray.direction;
        }
	}
}
